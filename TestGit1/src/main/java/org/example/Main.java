package org.example;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
         System.out.println("You have entered the calculation system");
        System.out.println("Please enter the numbers you want to calculate:");

        Scanner scanner = new Scanner(System.in);

        double a = scanner.nextDouble();
        double b = scanner.nextDouble();

        System.out.println("Which operation do you want to perform (+, -, *, /)?");
        char operation = scanner.next().charAt(0);

        Calculator calculator = new Calculator();
        double result;

        switch (operation) {
            case '+':
                result = calculator.add(a, b);
                System.out.println("result = " + Calculator.add(a,b));
                break;
            case '-':
                result = calculator.subtract(a, b);
                System.out.println("result = " + Calculator.subtract (a,b));
                break;
            case '*':
                result = calculator.multiply(a, b);
                System.out.println("result = " + Calculator.multiply(a,b));
                break;
            case '/':
                if (b == 0) {
                    System.out.println("Division by zero is not allowed.");
                } else {
                    result = calculator.divide(a, b);
                    System.out.println("result = " + Calculator.divide (a,b));
                }
                break;
            default:
                System.out.println("Please choose one of the addition, subtraction, multiplication, or division operations.");
        }

        scanner.close();
    }
}

class Calculator {


    public static double add(double a, double b) {
        return a + b;
    }

    public static double subtract(double a, double b) {
        return a - b;
    }

    public static double multiply(double a, double b) {
        return a * b;
    }

    public static double divide(double a, double b) {
        if (b == 0) {
            throw new ArithmeticException("Division by zero is not allowed.");
        }
        return a / b;
    }

    public double modulus(double a, double b) {
        if (b == 0) {
            throw new ArithmeticException("Division by zero is not allowed.");
        }
        return a % b;
    }
}

