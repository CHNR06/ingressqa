

public class Area {


    public static double circle(double radius) {
        return Math.PI * radius * radius;
    }


    public static double rectangle(double length, double width) {
        return length * width;
    }

    public static void main(String[] args) {

        double circleArea = Area.circle(5);
        System.out.println("the area of a circle = " + Area.circle(5));


        double rectangleArea = Area.rectangle(4, 6);
        System.out.println("the area of a rectangle =  " + Area.rectangle(4,6));




    }
}

