public class Factorial {

    public static void main(String[] args) {

        int factorial = factorial(3);
        System.out.println("factorial = " + factorial);
    }

    public static int factorial(int n) {

        if (n < 0) {
            return 0;
        }
        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact = fact * i;
        }
        return fact;
    }
}

