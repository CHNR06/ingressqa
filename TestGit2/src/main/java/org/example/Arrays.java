package org.example;

import java.util.Scanner;

public class Arrays {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int[] newArr = new int[arr.length - 1];

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the element to remove: ");
        int removeElement = sc.nextInt();

        int k = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != removeElement) {
                if (k < newArr.length) {
                    newArr[k++] = arr[i];
                }
            }
        }

        System.out.print("Array after removing " + removeElement + ": ");
        for (int i = 0; i < k; i++) {
            System.out.print(newArr[i] + " ");
        }
    }
}
